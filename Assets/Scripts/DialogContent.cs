﻿using UnityEngine;
using System.Collections;

public struct DialogContent {
	private string contents;
	private Color titleColor;

	public DialogContent(string contents,Color titleColor){
		this.contents = contents;
		this.titleColor = titleColor;
	}

	public string Contents{
		get{
			return contents;
		}
	}

	public Color TitleColor{
		get{
			return titleColor;
		}
	}
}

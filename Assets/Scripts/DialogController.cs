﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogController : MonoBehaviour {

	public Text contents;
	public Text title;
	private RectTransform trans;

	// Use this for initialization
	void Start () {
		trans = GetComponent<RectTransform> ();
		trans.localScale = Vector2.zero;
	}
	
	public void OpenDialog(DialogContent content){
		OpenAnimation ();
		contents.text = content.Contents;
		title.color = content.TitleColor;
		GamePlayManager.Instance.uiAnim.panelAnim.HidePanel ();
	}

	public void CloseDialog(){
		CloseAnimation ();
		GamePlayManager.Instance.uiAnim.panelAnim.ShowPanel ();
	}

	private void CloseAnimation(){
		UIAnimation.ItweenValueTo (gameObject, Vector2.one, Vector2.zero, .5f, iTween.EaseType.bounce, "ScaleDialog","CompleteAnimationDialogClosing");
	}

	private void OpenAnimation(){
		UIAnimation.ItweenValueTo (gameObject, Vector2.zero, Vector2.one, .5f, iTween.EaseType.bounce, "ScaleDialog");
	}

	private void CompleteAnimationDialogClosing(){
		GamePlayManager.Instance.CloseDialogNotify ();
	}
	private void CompleteAnimationDialog(){
		GamePlayManager.Instance.ActiveHandler ();
	}
	private void ScaleDialog(Vector2 value){
		trans.localScale = value;
	}
}

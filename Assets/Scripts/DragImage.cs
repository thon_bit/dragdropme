﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class DragImage : MonoBehaviour, IControlEventHandler {

	private Action<BaseEventData> BeginDragHandler;
	private Action<BaseEventData> DragHandler;
	private Action<BaseEventData> EndDragHandler;

	void Start(){
		GamePlayManager.Instance.AddListener (this);
	}

	void OnEnable(){
		//GamePlayManager.Instance.OnStartGame += ActivateEventHandler;
		GamePlayManager.Instance.OnResetGame += DeactivateEventHandler;
	}

	void OnDisable(){
		GamePlayManager instance = GamePlayManager.Instance;
		if (instance != null) {
			//instance.OnStartGame -= ActivateEventHandler;
			instance.OnResetGame -= DeactivateEventHandler;
		}
	}

	public void OnBeginDrag (BaseEventData eventData)
	{
		if (BeginDragHandler != null)
			BeginDragHandler (eventData);
	}

	public void Drag(BaseEventData eventData){
		if (DragHandler != null)
			DragHandler (eventData);
	}

	public void EndDrag(BaseEventData eventData){
		if (EndDragHandler != null)
			EndDragHandler (eventData);
	}

	private void SetPositionTempImage(BaseEventData eventData,RectTransform tempTrans){
		if (eventData is PointerEventData) {	
			Vector3 pointerPos = ((PointerEventData)eventData).position;
			tempTrans.position = pointerPos;
		}
	}

	private void ScaleAnimationSelectedImage(float fromVal, float toVal, string onCompleteMethod= null){
		UIAnimation.ItweenValueTo (gameObject, fromVal, toVal, .1f, iTween.EaseType.bounce, "SetScaleSelectedImage", onCompleteMethod);
	}

	private void DisableSelectedImage(){
		GamePlayManager.Instance.SetEnableTempImage (false);
	}

	private void SetScaleSelectedImage(float value){
		RectTransform trans = GamePlayManager.Instance.TempImage.GetComponent<RectTransform> ();
		trans.sizeDelta = new Vector2 (value, value);
	}

	private void AfterFinishRandomImage(){
		BeginDragHandler = (eventData) => {
			Image selectedImage = GetComponent<Image> ();
			if (selectedImage) {
				Image tempImage = GamePlayManager.Instance.TempImage.GetComponent<Image> ();
				GamePlayManager.Instance.SetEnableTempImage (true);
				tempImage.sprite = selectedImage.sprite;
				tempImage.SetNativeSize ();
				RectTransform trans = tempImage.GetComponent<RectTransform> ();
				trans.sizeDelta = Vector2.zero;
				SetPositionTempImage (eventData, trans);
				ScaleAnimationSelectedImage (50,120);
			}
		};

		DragHandler = (eventData) => {
			SetPositionTempImage (eventData, GamePlayManager.Instance.TempImage.GetComponent<RectTransform> ());
		};

		EndDragHandler = (eventData) => {
			ScaleAnimationSelectedImage (120, 50,"DisableSelectedImage");
		};
	}

	#region IControlEventHandler implementation

	public void ActivateEventHandler ()
	{
		AfterFinishRandomImage ();
	}

	public void DeactivateEventHandler ()
	{
		BeginDragHandler = null;
		DragHandler = null;
		EndDragHandler = null;
	}

	#endregion
}

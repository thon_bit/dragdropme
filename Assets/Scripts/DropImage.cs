﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class DropImage : MonoBehaviour, IControlEventHandler {

	private Action<BaseEventData> PointerEnterHandler;
	private Action<BaseEventData> PointerExitHandler;
	private Action<BaseEventData> DropHandler;

	public Image parent;
	public Color enterColor;

	private Image currentImage;
	private Color defaultColor;

	void Start(){
		currentImage = GetComponent<Image> ();
		defaultColor = parent.color;
		GamePlayManager.Instance.AddListener (this);
	}

	void OnEnable(){
		//GamePlayManager.Instance.OnStartGame += ActivateEventHandler;
		GamePlayManager.Instance.OnResetGame += DeactivateEventHandler;
	}

	void OnDisable(){
		GamePlayManager instance = GamePlayManager.Instance;
		if (instance != null) {
			//instance.OnStartGame -= ActivateEventHandler;
			instance.OnResetGame -= DeactivateEventHandler;
		}
	}

	public void PointerEnter(BaseEventData eventData){
		if (PointerEnterHandler != null)
			PointerEnterHandler (eventData);
	}

	public void PointerExit(BaseEventData eventData){
		if (PointerExitHandler != null)
			PointerExitHandler (eventData);
	}

	public void Drop(BaseEventData eventData){
		if (DropHandler != null)
			DropHandler (eventData);
	}

	private void AfterFinishRandomImage(){
		PointerEnterHandler = (eventData) => {
			if (GamePlayManager.Instance.IsSelectedImageActive) {
				parent.color = enterColor;
			}
		};

		PointerExitHandler = (eventData) => {
			parent.color = defaultColor;
		};

		DropHandler = (eventData) => {
			if (GamePlayManager.Instance.IsSelectedImageActive) {
				currentImage.sprite = GamePlayManager.Instance.TempImage.GetComponent<Image> ().sprite;
			}
		};

	}

	#region IControlEventHandler implementation

	public void ActivateEventHandler ()
	{
		AfterFinishRandomImage ();
	}

	public void DeactivateEventHandler ()
	{
		PointerEnterHandler = null;
		PointerExitHandler = null;
		DropHandler = null;
	}

	#endregion
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class GamePlayManager : MonoBehaviour {

	public event System.Action OnResetGame;
	public System.Action CloseDialogCallback;

	public Transform Canvas;
	public UIAnimation uiAnim;
	public ShuffleImages shuffleImg;
	public DialogController dialog;

	private GameObject tempImage;
	private List<IControlEventHandler> eventList;
	private static GamePlayManager instance;

	#region Mono
	void Start(){
		eventList = new List<IControlEventHandler> ();
	}

	void OnEnable(){
		uiAnim.startGameAnim.onCompleteAnimation += CompleteAnimation;
		shuffleImg.OnCompleteShuffle += uiAnim.startGameAnim.Play;
	}

	void OnDisable(){
		uiAnim.startGameAnim.onCompleteAnimation -= CompleteAnimation;
		shuffleImg.OnCompleteShuffle -= uiAnim.startGameAnim.Play;
	}
	#endregion

	public static GamePlayManager Instance{
		get{
			if (!instance) {
				instance = FindObjectOfType<GamePlayManager> ();
			}
			return instance;
		}
	}

	public void AddListener(IControlEventHandler handler){
		eventList.Add (handler);
	}

	public void ActiveHandler(){
		foreach (IControlEventHandler handler in eventList) {
			handler.ActivateEventHandler ();
		}
	}

	public void DeactiveHandler(){
		foreach (IControlEventHandler handler in eventList) {
			handler.DeactivateEventHandler ();
		}
	}

	#region Temp Image
	public GameObject TempImage{
		get{
			if (!tempImage) {
				tempImage = new GameObject ("tempImage");
				Image image = tempImage.AddComponent<Image> ();
				image.raycastTarget = false;
				tempImage.transform.SetParent (Canvas);
				tempImage.transform.localScale = Vector3.one;
				tempImage.SetActive (false);
			}
			return tempImage;
		}
	}

	public bool IsSelectedImageActive{
		get{
			return TempImage.activeSelf;
		}
	}

	public void SetEnableTempImage(bool enable){
		this.TempImage.SetActive (enable);
	}
	#endregion

	public void ActiveAfterCloseDialog(){
		CloseDialogCallback = ActiveHandler;
	}

	public void ResetAfterCloseDialog(){
		CloseDialogCallback = ()=>{
			if(OnResetGame!=null){
				OnResetGame();
			}
		};
	}

	public void OpenDialog(DialogContent content){
		DeactiveHandler ();
		dialog.OpenDialog (content);
	}
		
	private void CompleteAnimation(){
		ActiveHandler ();
	}

	public void CloseDialogNotify(){
		if (CloseDialogCallback != null) {
			CloseDialogCallback ();
		}
	}
		

}

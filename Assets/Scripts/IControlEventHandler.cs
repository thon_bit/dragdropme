﻿using UnityEngine;
using System.Collections;

public interface IControlEventHandler {
	void ActivateEventHandler ();
	void DeactivateEventHandler ();
}

﻿using UnityEngine;
using System.Collections;

public class ResultManager : MonoBehaviour, IControlEventHandler {

	private System.Action ResultHandler;

	void Start(){
		ActivateEventHandler ();
		GamePlayManager.Instance.AddListener (this);
	}

	#region Result
	public void CompareResult(){
		if (ResultHandler != null)
			ResultHandler ();
	}

	#endregion
	#region IControlEventHandler implementation

	public void ActivateEventHandler ()
	{
		ResultHandler = () => {
			DialogContent content;
			if (GamePlayManager.Instance.shuffleImg.IsImageEmpty) {
				content = new DialogContent ("Drop Image To Both Left and Right Box!!!!!   Try Again", Color.yellow);
				GamePlayManager.Instance.ActiveAfterCloseDialog();
			} else {
				if (GamePlayManager.Instance.shuffleImg.IsMatchImage) {
					content = new DialogContent ("Awesome!!!!! You Did It.", Color.green);
				} else {
					content = new DialogContent ("Oooops!!!!! Wrong Match.", Color.red);
				}
				GamePlayManager.Instance.ResetAfterCloseDialog();
			}
			GamePlayManager.Instance.OpenDialog(content);
		};
	}

	public void DeactivateEventHandler ()
	{
		ResultHandler = null;
	}

	#endregion
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ShuffleImages : MonoBehaviour {

	public event System.Action OnCompleteShuffle;

	public Sprite[] randomSprites;
	public Image randomImageLeft;
	public Image randomImageRight;
	public Dictionary<string,Sprite> randomImageList;
	public float randomTime;
	public float delayRandom;
	public float startTime;

	private int randomSpriteCount;
	private bool canShuffle;

	void Start(){
		Init ();
	}

	void OnEnable(){
		GamePlayManager.Instance.OnResetGame += Init;
	}

	void OnDisable(){
		GamePlayManager instance = GamePlayManager.Instance;
		if (instance != null)
			instance.OnResetGame -= Init;
	}

	#region Shuffle Images
	public bool IsMatchImage{
		get{
			return randomImageLeft.sprite.name.Equals (randomImageList ["left"].name) && randomImageRight.sprite.name.Equals (randomImageList ["right"].name);
		}
	}
	public bool IsImageEmpty{
		get{
			return randomImageLeft.sprite == null || randomImageRight.sprite == null;
		}
	}

	private void Init(){
		randomImageList = new Dictionary<string, Sprite> ();
		randomSpriteCount = randomSprites.Length;
		canShuffle = true;
		RandomImages ();
	}

	private void RandomImages(){
		GenerateRandomEachImage (randomImageLeft, "left");
		GenerateRandomEachImage (randomImageRight, "right");
		StartShuffleImage ();
	}

	private void GenerateRandomEachImage(Image img,string key){
		int index = Random.Range (0, randomSpriteCount);
		Sprite sprite = null;
		if (!randomImageList.TryGetValue (key, out sprite)) {
			sprite = randomSprites [index];
			randomImageList.Add (key, sprite);
		}
	}

	private void StartShuffleImage(){
		StartCoroutine (ShuffleImage ());
		Invoke ("StopShuffle", randomTime);
	}

	private void StopShuffle(){
		canShuffle = false;
	}

	private IEnumerator ShuffleImage(){
		int i = 0;
		while (canShuffle) {
			randomImageLeft.sprite = randomSprites [i];
			randomImageRight.sprite = randomSprites [i];
			i++;
			if (i >= randomSpriteCount)
				i = 0;
			yield return new WaitForSeconds (delayRandom);
		}
		randomImageLeft.sprite = randomImageList["left"];
		randomImageRight.sprite = randomImageList["right"];
		Invoke ("StartGame", startTime);
	}

	private void StartGame(){
		randomImageLeft.sprite = null;
		randomImageRight.sprite = null;
		//startGameAnim.Play ();
		if (OnCompleteShuffle != null) {
			OnCompleteShuffle ();
		}
	}

	#endregion
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class UIAnimation : MonoBehaviour {

	public StartGameAnimation startGameAnim;
	public PanelAnimation panelAnim;
	void Start(){
		startGameAnim.Start (this);
		panelAnim.Start (this);
	}

	void OnEnable(){
		startGameAnim.OnEnable ();
	}

	void OnDisable(){
		startGameAnim.OnDisable ();
	}

	#region StartGameAnimaton methods
	void MovingStart(Vector2 value){
		startGameAnim.MovingStart (value);
	}

	void MovingGame(Vector2 value){
		startGameAnim.MovingGame (value);
	}

	void FadeItween(){
		startGameAnim.FadeItween ();
	}

	void FadeLabel(float value){
		startGameAnim.FadeLabel (value);
	}

	void CompleteAnimation(){
		startGameAnim.CompleteAnimation ();
	}

	void ScaleButtonSubmit(Vector2 value){
		startGameAnim.ScaleButtonSubmit (value);
	}
	#endregion

	#region Panel Animation 
	void MoveUpperPanel(Vector2 value){
		panelAnim.MoveUpperPanel (value);
	}

	void MoveLeftPanel(Vector2 value){
		panelAnim.MoveLeftPanel (value);
	}

	void MoveRightPanel(Vector2 value){
		panelAnim.MoveRightPanel (value);
	}

	void CompleteMovingPanel(){
		panelAnim.CompleteMovingPanel ();
	}
	#endregion

	[Serializable]
	public class PanelAnimation{
		public RectTransform upperPanel;
		public RectTransform leftPanel;
		public RectTransform rightPanel;
		public Vector2 upperPanelHidePos;
		public Vector2 leftPanelHidePos;
		public Vector2 rightPanelHidePos;

		private UIAnimation UIAnim;
		private Vector2 upperPanelShownPos;
		private Vector2 leftPanelShownPos;
		private Vector2 rightPanelShownPos;

		public void Start(UIAnimation anim){
			UIAnim = anim;
			upperPanelShownPos = upperPanel.localPosition;
			leftPanelShownPos = leftPanel.localPosition;
			rightPanelShownPos = rightPanel.localPosition;
			upperPanel.localPosition = upperPanelHidePos;
			leftPanel.localPosition = leftPanelHidePos;
			rightPanel.localPosition = rightPanelHidePos;
			ShowPanel ();
		}

		public void ShowPanel(){
			//GamePlayManager.Instance.DeactiveHandler ();
			UIAnimation.ItweenValueTo (UIAnim.gameObject, upperPanelHidePos, upperPanelShownPos, .5f, iTween.EaseType.easeInBack, "MoveUpperPanel","CompleteMovingPanel");
			UIAnimation.ItweenValueTo (UIAnim.gameObject, leftPanelHidePos, leftPanelShownPos, .6f, iTween.EaseType.easeInBack, "MoveLeftPanel");
			UIAnimation.ItweenValueTo (UIAnim.gameObject, rightPanelHidePos, rightPanelShownPos, .6f, iTween.EaseType.easeInBack, "MoveRightPanel");
		}

		public void HidePanel(){
			//GamePlayManager.Instance.DeactiveHandler ();
			UIAnimation.ItweenValueTo (UIAnim.gameObject, upperPanelShownPos, upperPanelHidePos, .5f, iTween.EaseType.easeInBack, "MoveUpperPanel","CompleteMovingPanel");
			UIAnimation.ItweenValueTo (UIAnim.gameObject, leftPanelShownPos, leftPanelHidePos, .6f, iTween.EaseType.easeInBack, "MoveLeftPanel");
			UIAnimation.ItweenValueTo (UIAnim.gameObject, rightPanelShownPos, rightPanelHidePos, .6f, iTween.EaseType.easeInBack, "MoveRightPanel");
		}

		public void MoveUpperPanel(Vector2 value){
			upperPanel.localPosition = value;
		}

		public void MoveLeftPanel(Vector2 value){
			leftPanel.localPosition = value;
		}

		public void MoveRightPanel(Vector2 value){
			rightPanel.localPosition = value;
		}

		public void CompleteMovingPanel(){
			//GamePlayManager.Instance.ActiveHandler ();
		}
	}

	[Serializable]
	public class StartGameAnimation{

		public event Action onCompleteAnimation;
		public Vector2 startTargetPos;
		public Vector2 gameTargetPos;
		public RectTransform startGO;
		public RectTransform gameGo;
		public RectTransform submitButton;

		private Vector2 startDefaultPos;
		private Vector2 gameDefaultPos;
		private Image startImage;
		private Image gameImage;
		private Color startDefaultColor;
		private Color gameDefaultColor;
		private UIAnimation UIAnim;

		public void Start(UIAnimation anim){
			startDefaultPos = startGO.localPosition;
			gameDefaultPos = gameGo.localPosition;
			submitButton.localScale = Vector2.zero;
			startImage = startGO.GetComponent<Image> ();
			gameImage = gameGo.GetComponent<Image> ();
			startDefaultColor = startImage.color;
			gameDefaultColor = gameImage.color;
			UIAnim = anim;
		}

		public void OnEnable(){
			GamePlayManager.Instance.OnResetGame += Reset;
		}

		public void OnDisable(){
			GamePlayManager instance = GamePlayManager.Instance;
			if (instance != null)
				instance.OnResetGame -= Reset;
		}

		public void Play(){
			UIAnimation.ItweenValueTo (UIAnim.gameObject, startDefaultPos, startTargetPos, .5f, iTween.EaseType.bounce, "MovingStart", "FadeItween");
			UIAnimation.ItweenValueTo (UIAnim.gameObject, gameDefaultPos, gameTargetPos, .5f, iTween.EaseType.bounce, "MovingGame");
		}

		private void Reset(){
			submitButton.localScale = Vector2.zero;
			startImage.color = startDefaultColor;
			gameImage.color = gameDefaultColor;
			startGO.localPosition = startDefaultPos;
			gameGo.localPosition = gameDefaultPos;
		}

		public void FadeItween(){
			UIAnimation.ItweenValueTo (UIAnim.gameObject, 1, 0, .5f, iTween.EaseType.linear, "FadeLabel", "CompleteAnimation");
			ScaleButtonItween ();
		}

		private void ScaleButtonItween(){
			UIAnimation.ItweenValueTo (UIAnim.gameObject, new Vector2 (.5f, .5f), Vector2.one, .5f, iTween.EaseType.bounce, "ScaleButtonSubmit");
		}

		public void ScaleButtonSubmit(Vector2 value){
			submitButton.localScale = value;
		}

		public void CompleteAnimation(){
			if (onCompleteAnimation != null)
				onCompleteAnimation ();
		}

		public void FadeLabel(float value){
			Color startColor = startImage.color;
			startColor.a = value;
			Color gameColor = gameImage.color;
			gameColor.a = value;
			startImage.color = startColor;
			gameImage.color = gameColor;
		}

		public void MovingStart(Vector2 value){
			startGO.localPosition = value;
		}

		public void MovingGame(Vector2 value){
			gameGo.localPosition = value;
		}
	}


	public static void ItweenValueTo(GameObject gO,System.Object fromV,System.Object toV,float time
		,iTween.EaseType easeType,string updateMethod,string completeMethod = null){

		Hashtable hash = iTween.Hash (
			"from", fromV,
			"to", toV,
			"time", time,
			"easetype", easeType,
			"onupdate", updateMethod
		);
		if (completeMethod != null)
			hash.Add ("oncomplete", completeMethod);
		iTween.ValueTo (gO, hash);
	}

}
